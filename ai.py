"""
This is the file that should be used to code your AI.
"""
import random
from planar import Vec2

from game.models import *


class AI:
    def __init__(self):
        pass

    def step(self, game: Game):
        """
        Given the state of the 'game', decide what your cells ('game.me.cells')
        should do.

        :param game: Game object
        """

        print("Tick #{}".format(game.tick))

        self.game = game

        best_food = None
        best_food_weigth = 0

        if self.should_trade():
            return

        excludedCells = set()

        for player in game.players:
            if player == game.me:
                continue

            for cell in player.cells:
                for mycell in self.game.me.cells:
                    if self.can_eat_cell(cell, mycell):
                        target = Vec2(cell.target * 100)
                        mycell.move(target)
                        excludedCells.add(mycell)
                        break

                    weight = self.get_ennemy_weight(cell, mycell)

                    if best_food is None or weight > best_food_weigth:
                        best_food = self.find_target_ennemy(cell)
                        best_food_weigth = weight

        for virus in game.viruses:
            for mycell in self.game.me.cells:
                if mycell not in excludedCells:
                    if self.can_destroy_cell(virus, mycell):
                        target = Vec2(-mycell.target.y, mycell.target.x) * 100
                        print(target)
                        mycell.move(target)
                        excludedCells.add(mycell)

        # resources
        for resource in game.resources.gold:
            for
            weight = self.get_food_weight(resource, "gold")

            if best_food is None or weight > best_food_weigth:
                best_food = resource
                best_food_weigth = weight

        for resource in game.resources.silver:
            weight = self.get_food_weight(resource, "silver")

            if best_food is None or weight > best_food_weigth:
                best_food = resource
                best_food_weigth = weight

        for resource in game.resources.regular:
            weight = self.get_food_weight(resource, "regular")

            if best_food is None or weight > best_food_weigth:
                best_food = resource
                best_food_weigth = weight

        for mycell in game.me.cells:
            target = best_food
            mycell.move(target)

    def get_max_speed(self, cell):
        return max(100 - cell.mass * 0.05, 50)

    def can_eat_me(self, cell):
        for c in self.game.me.cells:
            if c.mass < 1.1 * cell.mass and c.position.distance_to(cell.position) < 1.5 * (cell.radius + c.radius):
                return True

        return False

    def can_eat_cell(self, predator, target):
        if target.mass < 1.1 * predator.mass and predator.position.distance_to(target.position) < 1.5 * (
                target.radius + predator.radius):
            return True

    def get_food_weight(self, resource, type):
        if type == "regular":
            return 900 / resource.distance_to(self.game.me.cells[0].position)

        if type == "silver":
            return 2500 / resource.distance_to(self.game.me.cells[0].position)

        if type == "gold":
            return 3500 / resource.distance_to(self.game.me.cells[0].position)

    def get_ennemy_weight(self, cell, mycell):
        max_weight = 0
        if mycell.mass > 1.1 * cell.mass:
            max_weight = max(max_weight, 4000 / cell.position.distance_to(mycell.position))
        return max_weight

    def can_destroy_cell(self, virus, mycell):
        if mycell.mass < 1.1 * virus.mass and mycell.position.distance_to(virus.position) < 2 * mycell.radius:
            print("VIRUS")
            return True
        return False

    def should_trade(self):
        if self.game.tick % 150 == 0:
            biggest_cell = None
            for cell in self.game.me.cells:
                if biggest_cell is None or cell.mass > biggest_cell.mass:
                    biggest_cell = cell

            print(biggest_cell)

            if biggest_cell.mass > 100:
                biggest_cell.trade(int(biggest_cell.mass * 0.10))
                return True
            if biggest_cell.mass > 200:
                biggest_cell.trade(int(biggest_cell.mass * 0.15))
                return True
            if biggest_cell.mass > 300:
                biggest_cell.trade(int(biggest_cell.mass * 0.40))
                return True
            if biggest_cell.mass > 400:
                biggest_cell.trade(int(biggest_cell.mass * 0.60))
                return True

            return True
        return False

    def find_target_ennemy(self, cell):
        return cell.position * 0.10
